import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_mail(loginDataConfig, body):


    # print(loginDataConfig['app_confiuration'])
    for row in loginDataConfig['app_confiuration']:
        # print(row['email_notify_from'])
        fromaddr = row['email_notify_from']
        toaddr = row['email_notify_to']
        email_id = row['email_notify_server']
        password = row['email_notify_password']
        subject = row['subject']

    # fromaddr = "app_notifications@yuktamedia.com"
    # toaddr = 'ankit.hegde@yuktamedia.com'
    # tocc = 'priyanka.chaudhari@yuktamedia.com,ashish.bhosale@yuktamedia.com'
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    # msg['Cc'] = tocc
    msg['Subject'] = subject

    body = body
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP(email_id, 587)
    server.starttls()
    server.login(fromaddr, password)
    text = msg.as_string()
    # print(text)
    server.sendmail(fromaddr, toaddr.split(","), text)
    server.quit()

# send_mail("Hello","How are you? Tea la kadi zaych?")

import os
import shutil
import sys
from distutils.core import setup
from distutils.extension import Extension

from Cython.Distutils import build_ext

exclude_dir = [
               ".idea",
               ".git",
               "build",
               "__pycache__",
               "test",
               "un-used"]

exclude_files = [".gitignore", "email_config.json","mail_details_test.json", "compile.py","log_message.pyc", "log_message.py","search_download.pyc", "download_attachment.pyc","email_config.json"]

uncompile_files = ["start_me.py"]
include = []
exclude = []
current_dir = str(os.getcwd())

for f in os.listdir('.'):
    if os.path.isfile(f):
        if f not in exclude_files:
            if ".py" in f and f not in uncompile_files:
                include.append(f)
            else:
                exclude.append(f)
    else:
        for dirpath, dirname, filenames in os.walk(f):
            valid = True
            for dir_name in exclude_dir:
                if dir_name in dirpath:
                    valid = False
            for ef_file in exclude_files:
                if ef_file in filenames:
                    valid = False
            if valid:
                for file in filenames:
                    if '.iml' not in file:
                        if ".py" in file and file not in uncompile_files:
                            include.append(os.path.join(str(dirpath), file))
                        else:
                            exclude.append(os.path.join(str(dirpath), file))

ext_modules = []

for x in include:
    ext_modules.append(Extension(
        os.path.join("Imaplib", x).replace(os.sep, ".", 100).replace(".py", ""), [x]))

setup(
    name='Imaplib',
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules,

)
for x in include:
    try:
        os.remove(x.replace(".py", ".c"))

    except Exception as e:
        print(e)

for x in exclude:
    parent_dir_path = os.path.abspath(os.path.join(x, '..'))
    dts1 = "Imaplib" + str(parent_dir_path).replace(current_dir, "")
    dts = os.path.join("Imaplib",x)
    #os.mkdir(dts1)
    try:
        os.makedirs(dts1)
    except OSError, e:
        if e.errno != os.errno.EEXIST:
            raise  
        pass    
    print('dts',dts)
    shutil.copyfile(x, dts)
    print(x)
